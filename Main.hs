module Main where

import qualified GDriveFuse
import qualified Network.GData.ClientUtils as GDataUtils
import qualified Network.GData.Authorization as GAuth
import qualified Data.ByteString.Char8 as BS

auth = GAuth.Authorization {
  GAuth.clientId = BS.pack "563777513187.apps.googleusercontent.com"
  ,GAuth.clientSecret = BS.pack "SkWa1_me74dfflKTEr_IFHMO"
  ,GAuth.redirectUrl = BS.pack "urn:ietf:wg:oauth:2.0:oob"
  ,GAuth.authorizationScope = GAuth.Drive
}

f = "token.dat"
loadToken = GDataUtils.loadToken auth f

main = loadToken GDriveFuse.mount failure

failure = do
  putStrLn "Unable to get GData access token"
