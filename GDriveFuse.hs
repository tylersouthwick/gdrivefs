module GDriveFuse(mount) where

import Control.Exception.Base
import System.Fuse
import Data.Time.Clock
import Data.Time.Calendar
import Foreign.C.Error
import Data.Time.Clock.POSIX

fuseOperations = defaultFuseOps {
  fuseReadDirectory = readDirectory
  ,fuseGetFileStat = getFileStat
  ,fuseOpen = openFile
  ,fuseOpenDirectory = openDirectory
}

mount auth = fuseMain fuseOperations converter

converter :: SomeException -> IO Errno
converter = defaultExceptionHandler

currentTime = do
  currentTime <- getCurrentTime
  let time = fromIntegral . fromEnum . utcTimeToPOSIXSeconds $ currentTime
  return time

openFile :: FilePath -> OpenMode -> OpenFileFlags -> IO (Either Errno fh)
openFile filePath openMode openFileFlags = do
  return (Left eCONNREFUSED)

openDirectory :: FilePath -> IO Errno
openDirectory filePath = do
  return eOK

readDirectory :: FilePath -> IO (Either Errno [(FilePath, FileStat)])
readDirectory filePath = do
  time <- currentTime
  return (emptyFile time)

getFileStat :: FilePath -> IO (Either Errno FileStat)
getFileStat filePath = do
  time <- currentTime
  return (Right (emptyFileStat time))

emptyFile time = Right [("test" :: FilePath, emptyFileStat time)]

emptyFileStat time = FileStat {
  statEntryType = Directory
  ,statFileMode = 0
  ,statLinkCount = 0
  ,statFileOwner = 0
  ,statFileGroup = 0
  ,statSpecialDeviceID = 0
  ,statFileSize = 0
  ,statBlocks = 0
  ,statAccessTime = time
  ,statModificationTime = time
  ,statStatusChangeTime = time
}
